#!/bin/bash

base_dir="$(dirname $(readlink -f $0))"
bin_dir="$base_dir/bin"
src_dir="$base_dir/src"

mkdir -p $bin_dir
sources=$(find $src_dir -name '*.java')

javac -d $bin_dir $sources
