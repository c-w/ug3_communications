import java.net.InetAddress;

import udp.Client;


public class Sender1 extends Client {

    public static void main(String[] args) {
        Sender1 s1 = new Sender1(args);
        s1.run();
        s1.postRun();
    }

    public Sender1(String[] args) {
        super(args);
    }

    /** send packets without any sort of server/client communication **/
    public void run() {
        super.run();
        int num_packets = application_buffer.size();
        for (int i = 0; i < num_packets; i++) {
            byte[] packet = makePacket(application_buffer.get(i), i);
            vmessage("sending packet %d/%d", i, num_packets - 1);
            sendPacket(packet);
        }
    }

    @Override protected void sendPacket(byte[] data, int port, InetAddress ip) {
        super.sendPacket(data, port, ip);
        // make sure we give the receiver enough time to process the packet
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
