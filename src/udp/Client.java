package udp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import utils.ArrayUtils;
import utils.FileUtils;


public abstract class Client extends UDPObject {
    public static final File DEFAULT_INPUT = new File("../res/small_testfile.jpg");
    public static final String DEFAULT_HOST = "localhost";
    public static final int DEFAULT_RETRY_TIMEOUT = 50;

    /** file to send **/
    protected File input;
    /** re-send packets if an acknowledgement is not received within this time **/
    protected int retry_timeout;
    /** host to use **/
    protected String host;

    /** time at which the client started sending data - for throughput **/
    protected Long transmission_start_time;

    /**
     * Create a new client with some specified parameters.
     * Read an image.
     * Fragment it.
     * Send the fragments over some port.
     *
     * @param args Array of space-separated server configuration options
     * <pre>
     *                args[0] => host name
     *                args[1] => port number
     *                args[2] => image file
     *                args[3] => retry timeout
     *                args[4] => window size
     * </pre>
     */
    public Client(String[] args) {
        super(args);
        host = DEFAULT_HOST;
        input = DEFAULT_INPUT;
        retry_timeout = DEFAULT_RETRY_TIMEOUT;

        /* parse arguments */
        if (args.length >= 5) {
            try {
                window_size = Integer.parseInt(args[4]);
            } catch (NumberFormatException e) {
                error(ExitCode.BAD_ARGUMENT, "window-size is not an integer");
            }
        } else {
            warning("using default window-size %d", DEFAULT_WINDOW_SIZE);
        }
        if (args.length >= 4) {
            retry_timeout = Integer.parseInt(args[3]);
        } else {
            warning("using default retry-timeout %d", DEFAULT_RETRY_TIMEOUT);
        }
        if (args.length >= 3) {
            input = new File(args[2]);
            if (!input.exists()) {
                error(ExitCode.BAD_ARGUMENT, "input file not found");
            }
        } else {
            warning("using default input file %s", DEFAULT_INPUT);
        }
        if (args.length >= 2) {
            try {
                port = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                error(ExitCode.BAD_ARGUMENT, "port-number is not an integer");
            }
        } else {
            warning("using default port-number %d", DEFAULT_PORT);
        }
        if (args.length >= 1) {
            host = args[0];
        } else {
            warning("using default host %s", DEFAULT_HOST);
        }

        /* set up locals */
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    protected void input2bytes() {
        byte[] input_bytes = null;
        try {
            input_bytes = FileUtils.file2bytes(input);
        } catch (FileNotFoundException e) {
            error(ExitCode.CRITICAL_EXCEPTION, "input not found");
        } catch (IOException e) {
            e.printStackTrace();
            error(ExitCode.CRITICAL_EXCEPTION, "exception while reading input");
        }
        application_buffer = ArrayUtils.array2chunks(input_bytes, PAYLOAD_LEN);
    }

    protected byte[] makePacket(byte[] data, int seq_num) {
        PacketType packet_type;
        if (seq_num == application_buffer.size() - 1) {
            packet_type = PacketType.EOF;
        } else {
            packet_type = PacketType.NORMAL;
        }
        return makePacket(data, seq_num, packet_type);
    }

    protected Double calculateThroughput() {
        if (transmission_start_time == null) {
            warning("forgot to start throughput timer");
            return null;
        }
        double time_s = (System.currentTimeMillis() - transmission_start_time.longValue()) * 0.001;
        double size_kb = application_buffer.size() * PACKET_LEN / 1024;
        return Double.valueOf(size_kb / time_s);
    }

    @Override public void run() {
        vmessage("splitting image into packets");
        input2bytes();
        vmessage("ready for transmission");
        transmission_start_time = System.currentTimeMillis();
    }
    @Override protected void postRun() {
        message("throughput = %f KB/s", calculateThroughput());
    }

    @Override public int getSendToPort() {
        return port;
    }
    @Override public InetAddress getSendToAddress() {
        InetAddress send_to = null;
        try {
            send_to = InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return send_to;
    }
    @Override public int getNumMaxArgs() {
        return 5;
    }
    @Override public String getClassName() {
        return Thread.currentThread().getStackTrace()[1].getClassName();
    }
}
