package udp;

import java.io.File;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import utils.ArrayUtils;
import utils.FileUtils;


public abstract class Server extends UDPObject {
    public static final File DEFAULT_OUTPUT = new File("./output.jpg");

    /** file to write received packets to **/
    protected File output;

    /**
     * Create a new server with some specified parameters.
     * Listen for image fragment packets on some port.
     * Once the last fragment has been received, reconstruct the image.
     *
     * @param args Array of space-separated server configuration options
     * <pre>
     *                args[0] => port number
     *                args[1] => output file
     *                args[2] => window size
     * </pre>
     */
    public Server(String[] args) {
        super(args);
        output = DEFAULT_OUTPUT;

        /* parse arguments */
        if (args.length >= 3) {
            try {
                window_size = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                error(ExitCode.BAD_ARGUMENT, "window-size is not an integer");
            }
        } else {
            warning("using default window-size %d", DEFAULT_WINDOW_SIZE);
        }
        if (args.length >= 2) {
            output = new File(args[1]);
        } else {
            warning("using default output-file %s", DEFAULT_OUTPUT);
        }
        if (args.length >= 1) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                error(ExitCode.BAD_ARGUMENT, "port-number is not an integer");
            }
        } else {
            warning("using default port-number %d", DEFAULT_PORT);
        }

        /* set up locals */
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    /** keep data **/
    protected void deliverData(byte[] data) {
        application_buffer.add(data);
    }

    /** Assemble the received packets to a file and save it **/
    protected void packets2file() {
        byte[] bytes = ArrayUtils.chunks2array(application_buffer);
        try {
            FileUtils.bytes2file(bytes, output);
        } catch (Exception e) {
            e.printStackTrace();
            error(ExitCode.CRITICAL_EXCEPTION, "exception while writing output");
        }
    }

    @Override public void run() {
        vmessage("ready to receive");
    }
    @Override protected void postRun() {
        vmessage("EOF");
        packets2file();
    }

    @Override public int getSendToPort() {
        return packet_in.getPort();
    }
    @Override public InetAddress getSendToAddress() {
        if (packet_in == null) {
            warning("sending back to null packet");
            return null;
        }
        InetAddress send_to = packet_in.getAddress();
        return send_to;
    }
    @Override public int getNumMaxArgs() {
        return 3;
    }
    @Override public String getClassName() {
        return Thread.currentThread().getStackTrace()[1].getClassName();
    }
}
