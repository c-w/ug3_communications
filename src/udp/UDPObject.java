package udp;

import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;

import utils.ArrayUtils;
import utils.IntUtils;


public abstract class UDPObject extends Thread {
    public static final boolean __VERBOSE__ = true;

    /** protocol: header = | packet sequence number | packet type | **/
    public static final int HEADER_LEN = 5;
    public static final int HEADER_START = 0;
    public static final int HEADER_END = HEADER_START + HEADER_LEN;
    /** protocol: packet sequence number = | byte | byte | byte | byte | **/
    public static final int HEADER_SEQNUM_LEN = 4;
    public static final int HEADER_SEQNUM_START = HEADER_START;
    public static final int HEADER_SEQNUM_END = HEADER_SEQNUM_START + HEADER_SEQNUM_LEN;
    /** protocol: packet type = | byte | **/
    public static final int HEADER_TYPE_LEN = 1;
    public static final int HEADER_TYPE_START = HEADER_SEQNUM_END;
    public static final int HEADER_TYPE_END = HEADER_TYPE_START + HEADER_TYPE_LEN;
    /** protocol: packet pay-load = 1204 bytes **/
    public static final int PAYLOAD_LEN = 1024;
    public static final int PAYLOAD_START = HEADER_END;
    public static final int PAYLOAD_END = PAYLOAD_START + PAYLOAD_LEN;
    /** protocol: packet = | header | pay-load | **/
    public static final int PACKET_LEN = HEADER_LEN + PAYLOAD_LEN;
    /** protocol: number of required acknowledgements of last packet **/
    public static final int NUM_EOF_ACKS = 5;

    /** maximum number that fits into the header **/
    public static final int MAX_SEQ_NUM = (int) Math.pow(2, HEADER_SEQNUM_LEN * 4);

    /** protocol: possible packet types **/
    public static enum PacketType {
        NORMAL,
        EOF,
        ACK,
    }

    /** possible terminal states of program */
    public static enum ExitCode {
        SUCCESS,
        BAD_ARGUMENT,
        BAD_STATE,
        CRITICAL_EXCEPTION,
    }

    public static final int DEFAULT_PORT = 6789;
    public static final int DEFAULT_WINDOW_SIZE = 1;

    /** protocol port **/
    protected int port;
    /** window size for go-back-n and selective-repeat protocols **/
    protected int window_size;
    /** client/server send/receive socket **/
    protected DatagramSocket socket;
    /** the last received packet **/
    protected DatagramPacket packet_in;
    /** the data in {@link #packet_in} **/
    protected byte[] data_in;
    /** the last sent packet **/
    protected DatagramPacket packet_out;
    /** received/to send data store **/
    protected ArrayList<byte[]> application_buffer;

    public UDPObject(String[] args) {
        if (args.length > getNumMaxArgs()) {
            String[] unused_args = Arrays.copyOfRange(args, getNumMaxArgs(), args.length);
            warning("unused arguments: " + Arrays.toString(unused_args));
        }
        port = DEFAULT_PORT;
        window_size = DEFAULT_WINDOW_SIZE;
        data_in = new byte[PACKET_LEN];
        application_buffer = new ArrayList<byte[]>();
    }

    /** UDP receive **/
    protected void receivePacket() throws SocketTimeoutException {
        packet_in = new DatagramPacket(data_in, PACKET_LEN);
        try {
            getSocket().receive(packet_in);
        } catch (SocketTimeoutException e) {
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** UDP send some data to some address/port **/
    protected void sendPacket(byte[] data, int port, InetAddress ip) {
        DatagramPacket packet = new DatagramPacket(data, data.length, ip, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** UDP send some data some number of times to the default port/address **/
    protected void sendPacket(byte[] data, int num) {
        for (int i = 0; i < num; i++) {
            sendPacket(data, getSendToPort(), getSendToAddress());
        }
    }

    /** UDP send some data to the default port/address **/
    protected void sendPacket(byte[] data) {
        sendPacket(data, 1);
    }

    /** @return an acknowledgement-packet for packet ack_num **/
    protected byte[] makeACK(int ack_num) {
        byte[] data = new byte[PAYLOAD_LEN];
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) 0;
        }
        return makePacket(data, ack_num, PacketType.ACK);
    }

    /** @return a packet with some data, number, and type **/
    protected byte[] makePacket(byte[] data, int seq_num, PacketType packet_type) {
        byte[] header = generateHeader(seq_num % MAX_SEQ_NUM, packet_type);
        byte[] packet = ArrayUtils.chunks2array(Arrays.asList(header, data));
        return packet;
    }

    /**
     * @return a simple header | packet_number (2 bytes) | type (1 byte) |
     *            type can be any of {@link PacketType}
     */
    protected byte[] generateHeader(int packet_number, PacketType type) {
        packet_number = packet_number % MAX_SEQ_NUM;
        byte[] seq_num_bytes = IntUtils.int2bytes(Integer.valueOf(packet_number));
        byte[] type_bytes = new byte[]{ (byte) type.ordinal() };
        byte[] header = ArrayUtils.chunks2array(Arrays.asList(seq_num_bytes, type_bytes));
        return header;
    }

    /* analyse packets */
    protected byte[] extractPayload(byte[] receive_data) {
        return Arrays.copyOfRange(receive_data, PAYLOAD_START, packet_in.getLength());
    }
    protected byte[] extractPayload() {
        return extractPayload(data_in);
    }
    public static byte[] extractHeader(byte[] receive_data) {
        return Arrays.copyOfRange(receive_data, HEADER_START, HEADER_END);
    }
    protected byte[] extractHeader() {
        return extractHeader(data_in);
    }
    protected int extractSequenceNumber(byte[] receive_data) {
        byte[] header = extractHeader(receive_data);
        byte[] seq_num = Arrays.copyOfRange(header, HEADER_SEQNUM_START, HEADER_SEQNUM_END);
        int sequence_number = IntUtils.bytes2int(seq_num);
        return sequence_number;
    }
    protected int extractSequenceNumber() {
        return extractSequenceNumber(data_in);
    }
    protected int extractPacketType(byte[] received_data) {
        byte[] header = extractHeader(received_data);
        byte[] type_bytes = Arrays.copyOfRange(header, HEADER_TYPE_START, HEADER_TYPE_END);
        int type = IntUtils.bytes2int(type_bytes);
        return type;
    }
    protected boolean isEOF(byte[] receive_data) {
        return extractPacketType(receive_data) == PacketType.EOF.ordinal();
    }
    protected boolean isEOF() {
        return isEOF(data_in);
    }
    protected boolean isACK(int num, byte[] received_data) {
        if (extractPacketType(received_data) != PacketType.ACK.ordinal()) {
            return false;
        }
        return extractSequenceNumber(received_data) == num;
    }
    protected boolean isACK(int type) {
        return isACK(type, data_in);
    }

    /* input-output helper functions */
    protected void vmessage(String format, Object... args) {
        if (__VERBOSE__) { message(format, args); }
    }
    protected void message(String format, Object... args) {
        message(System.out, format, args);
    }
    protected void warning(String format, Object... args) {
        message(System.err, "warning: " + format, args);
    }
    protected void error(ExitCode exit_code, String format, Object... args) {
        message(System.err, "error: " + format, args);
        System.exit(exit_code.ordinal());
    }
    protected void message(PrintStream stream, String format, Object... args) {
        stream.printf("[%s] %s\n", getClassName(), String.format(format, args));
    }

    /* getters (most should be overwritten) */
    abstract protected void postRun();
    abstract public int getSendToPort();
    abstract public InetAddress getSendToAddress();
    abstract public int getNumMaxArgs();
    abstract public String getClassName();
    public DatagramSocket getSocket() {
        return socket;
    }
    public int getPort() {
        return port;
    }
    public int getWindowSize() {
        return window_size;
    }
}
