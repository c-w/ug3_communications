import java.net.SocketTimeoutException;

import udp.Server;


public class Receiver3 extends Server {

    /** last acknowledged packet **/
    protected int expected_seq_num;
    protected byte[] last_ack;

    public static void main(String[] args) {
        Receiver3 r3 = new Receiver3(args);
        r3.run();
        r3.postRun();
    }

    public Receiver3(String[] args) {
        super(args);
        expected_seq_num = 0;
    }

    /** receive packets according to the go-back-n protocol **/
    public void run() {
        super.run();
        while (true) {
            try {
                receivePacket();
            } catch (SocketTimeoutException e) {
                continue;
            }
            int seq_num = extractSequenceNumber();
            if (seq_num == expected_seq_num) {
                vmessage("got packet %d as expected, sending ACK", application_buffer.size());
                deliverData(extractPayload());
                last_ack = makeACK(expected_seq_num);
                sendPacket(last_ack);
                if (isEOF()) {
                    break;
                }
                expected_seq_num++;
            } else {
                vmessage("got unexpected packet %d - want %d", seq_num, expected_seq_num);
                sendPacket(last_ack);
            }
        }
        // re-send the last acknowledgement a number of times and hope one
        // makes it through
        sendPacket(last_ack, NUM_EOF_ACKS);
    }
}
