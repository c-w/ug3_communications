import java.net.SocketException;
import java.net.SocketTimeoutException;

import udp.Client;


public class Sender2 extends Client {

    /** possible states in the sender finite state machine **/
    public static enum ClientState {
        WAIT_FOR_CALL_FROM_ABOVE_0,
        WAIT_FOR_ACK_0,
        WAIT_FOR_CALL_FROM_ABOVE_1,
        WAIT_FOR_ACK_1,
    }
    /** current position in the sender finite state machine **/
    protected ClientState state;

    /** last sent packet **/
    protected byte[] packet;
    /** number of packets that had to be sent again **/
    protected int num_retransmissions;

    public static void main(String[] args) {
        Sender2 s2 = new Sender2(args);
        s2.run();
        s2.postRun();
    }

    public Sender2(String[] args) {
        super(args);
        state = ClientState.WAIT_FOR_CALL_FROM_ABOVE_0;
        num_retransmissions = 0;
        try {
            socket.setSoTimeout(retry_timeout);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    /** send packets according to the stop-and-wait protocol **/
    public void run() {
        super.run();
        int num_packets = application_buffer.size();
        for (int i = 0; i < num_packets;) {
            switch (state) {
                case WAIT_FOR_CALL_FROM_ABOVE_0:
                    vmessage("sending packet %d/%d", i, num_packets - 1);
                    packet = makePacket(application_buffer.get(i), i);
                    i++;
                    sendPacket(packet);
                    state = ClientState.WAIT_FOR_ACK_0;
                    break;

                case WAIT_FOR_ACK_0:
                    try {
                        receivePacket();
                    } catch (SocketTimeoutException e) {
                        vmessage("timeout: re-sending packet %d", i);
                        num_retransmissions++;
                        sendPacket(packet);
                        break;
                    }
                    if (isACK(0)) {
                        state = ClientState.WAIT_FOR_CALL_FROM_ABOVE_1;
                    }
                    break;

                case WAIT_FOR_CALL_FROM_ABOVE_1:
                    vmessage("sending packet %d/%d", i, num_packets - 1);
                    packet = makePacket(application_buffer.get(i), i);
                    i++;
                    sendPacket(packet);
                    state = ClientState.WAIT_FOR_ACK_1;
                    break;

                case WAIT_FOR_ACK_1:
                    try {
                        receivePacket();
                    } catch (SocketTimeoutException e) {
                        vmessage("timeout: re-sending packet %d", i);
                        num_retransmissions++;
                        sendPacket(packet);
                        break;
                    }
                    if (isACK(1)) {
                        state = ClientState.WAIT_FOR_CALL_FROM_ABOVE_0;
                    }
                    break;

                default:
                    error(ExitCode.BAD_STATE, "unhandled state");
                    break;
            }
        }
        // re-send the last packet a number of times and hope one makes it through
        sendPacket(application_buffer.get(num_packets - 1), NUM_EOF_ACKS);
    }

    @Override protected void postRun() {
        super.postRun();
        message("number of retransmissions: %d", num_retransmissions);
    }
}
