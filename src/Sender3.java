import java.net.SocketException;
import java.net.SocketTimeoutException;

import udp.Client;
import utils.Timer;


public class Sender3 extends Client {

    public static enum ClientState {
        SEND_REQUEST,
        RECEIVE_PACKET,
        TIMEOUT,
    }

    protected int next_seq_num;
    protected int packets_sent;
    protected int base;

    protected Timer timer;

    public static void main(String[] args) {
        Sender3 s3 = new Sender3(args);
        s3.run();
        s3.postRun();
    }

    public Sender3(String[] args) {
        super(args);
        try {
            socket.setSoTimeout(1);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        timer = new Timer(retry_timeout);
        next_seq_num = 0;
        base = 0;
        packets_sent = 0;
    }

    /** send packets according to the go-back-n protocol **/
    public void run() {
        super.run();
        int num_packets = application_buffer.size();
loop:    while (true) {
            switch (determineState()) {
                case RECEIVE_PACKET:
                    int ack_num = extractSequenceNumber();
                    vmessage("got ACK for %d", ack_num);
                    base = ack_num + 1;
                    if (base == next_seq_num) {
                        timer.stop();
                    } else {
                        timer.start();
                    }
                    if (ack_num == num_packets - 1) {
                        break loop;
                    }
                    break;

                case SEND_REQUEST:
                    if (next_seq_num < base + window_size && packets_sent < num_packets) {
                        vmessage("sending %d/%d", packets_sent, num_packets - 1);
                        byte[] data = application_buffer.get(packets_sent);
                        byte[] packet = makePacket(data, next_seq_num);
                        next_seq_num++;
                        packets_sent++;
                        sendPacket(packet);
                        timer.start();
                    }
                    break;

                case TIMEOUT:
                    timer.stop();
                    for (int i = base; i < next_seq_num; i++) {
                        byte[] resend_packet = makePacket(application_buffer.get(i), i);
                        vmessage("timeout, re-sending %d", extractSequenceNumber(resend_packet));
                        sendPacket(resend_packet);
                    }
                    timer.start();
                    break;

                default:
                    error(ExitCode.BAD_STATE, "unhandled state");
                    break;
            }
        }
    }

    protected ClientState determineState() {
        ClientState state = ClientState.SEND_REQUEST;
        try {
            receivePacket();
            state = ClientState.RECEIVE_PACKET;
        } catch (SocketTimeoutException e) {
            if (timer.timeout()) {
                state = ClientState.TIMEOUT;
            }
        }
        return state;
    }
}
