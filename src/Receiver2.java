import java.net.SocketTimeoutException;

import udp.Server;


public class Receiver2 extends Server {

    /** possible states in the receiver finite state machine **/
    public static enum ServerState {
        WAIT_FOR_0,
        WAIT_FOR_1,
    }
    /** current position in the receiver finite state machine **/
    protected ServerState state;

    public static void main(String[] args) {
        Receiver2 r2 = new Receiver2(args);
        r2.run();
        r2.postRun();
    }

    public Receiver2(String[] args) {
        super(args);
        state = ServerState.WAIT_FOR_0;
    }

    /** receive packets according to the stop-and-wait protocol **/
    public void run() {
        super.run();
        while (true) {
            try {
                receivePacket();
            } catch (SocketTimeoutException e) {
                continue;
            }
            int seq_num = extractSequenceNumber();
            switch (state) {
                case WAIT_FOR_0:
                    switch (seq_num) {
                        case 0:
                            vmessage("received packet %d", application_buffer.size());
                            deliverData(extractPayload());
                            sendPacket(makeACK(0));
                            state = ServerState.WAIT_FOR_1;
                            break;
                        case 1:
                            vmessage("drop");
                            sendPacket(makeACK(1));
                            break;
                        default:
                            error(ExitCode.BAD_STATE, "unhandled sequence number %d", seq_num);
                            break;
                        }
                    break;

                case WAIT_FOR_1:
                    switch (seq_num) {
                        case 0:
                            vmessage("drop");
                            sendPacket(makeACK(0));
                            break;
                        case 1:
                            vmessage("received packet %d", application_buffer.size());
                            deliverData(extractPayload());
                            sendPacket(makeACK(1));
                            state = ServerState.WAIT_FOR_0;
                            break;
                        default:
                            error(ExitCode.BAD_STATE, "unhandled sequence number %d", seq_num);
                            break;
                    }
                    break;

                default:
                    error(ExitCode.BAD_STATE, "unhandled state");
                    break;
            }
            if (isEOF()) {
                break;
            }
        }
    }

    @Override public int extractSequenceNumber() {
        int seq_num = super.extractSequenceNumber();
        return seq_num % 2;
    }
}
