import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import udp.Client;
import utils.Timer;


public class Sender4 extends Client {

    protected HashMap<Integer, Timer> packet_timers;
    protected int send_base;
    protected boolean[] acknowledged;
    protected boolean[] sent;

    public static void main(String[] args) {
        Sender4 s4 = new Sender4(args);
        s4.run();
        s4.postRun();
    }

    public Sender4(String[] args) {
        super(args);
        try {
            socket.setSoTimeout(1);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        send_base = 0;
        packet_timers = new HashMap<Integer, Timer>();
    }

    /** send packets according to the selective-repeat protocol **/
    @Override public void run() {
        super.run();
        acknowledged = new boolean[application_buffer.size()];
        sent = new boolean[application_buffer.size()];
        while (true) {
            try {
                receivePacket();
                // ACK received
                int ack_num = extractSequenceNumber();
                vmessage("got ACK for %d", ack_num);
                if (ack_num == application_buffer.size() - 1) {
                    break;
                }
                if (ack_num >= send_base && ack_num <= send_base + window_size) {
                    // packet is in the window
                    acknowledged[ack_num] = true;
                    if (ack_num == send_base) {
                        // move window to smallest non-acknowledged position
                        while (acknowledged[send_base]) {
                            send_base++;
                            vmessage("move window to %d", send_base);
                        }
                        if (send_base != ack_num) {
                            // the window moved: send any not transmitted packets in new window
                            for (int i = send_base; i < send_base + window_size; i++) {
                                if (!sent[i]) {
                                    vmessage("sending %d because of window move", i);
                                    sendPacket(i);
                                }
                            }
                        }
                    }
                }
                continue;
            } catch (SocketTimeoutException e) {
                Integer timeout_packet = getExpiredPacket();
                if (timeout_packet != null) {
                    // timeout - retransmit expired packet
                    vmessage("sending %d because of timer", timeout_packet);
                    if (!acknowledged[timeout_packet]) {
                        sendPacket(timeout_packet);
                    }
                } else {
                    // data from above event - find next available sequence number
                    for (int i = send_base; i < send_base + window_size; i++) {
                        if (!sent[i]) {
                            vmessage("sending %d", i);
                            sendPacket(i);
                            break;
                        }
                    }
                    vmessage("window full");
                }
            }
        }
    }

    protected Integer getExpiredPacket() {
        Integer timeout_packet = null;
        for (Integer packet : packet_timers.keySet()) {
            Timer timer = packet_timers.get(packet);
            if (timer.timeout()) {
                timer.stop();
                timeout_packet = packet;
                break;
            }
        }
        return timeout_packet;
    }

    protected void sendPacket(int num) {
        byte[] data = application_buffer.get(num);
        sent[num] = true;
        Timer timer = new Timer(retry_timeout);
        timer.start();
        packet_timers.put(Integer.valueOf(num), timer);
        sendPacket(makePacket(data, num));
    }
}
