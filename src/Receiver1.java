import java.net.SocketTimeoutException;

import udp.Server;


public class Receiver1 extends Server {

    protected int received_packets;

    public static void main(String[] args) {
        Receiver1 r1 = new Receiver1(args);
        r1.run();
        r1.postRun();
    }

    public Receiver1(String[] args) {
        super(args);
        received_packets = 0;
    }

    /** receive packets without any sort of server/client communication **/
    public void run() {
        super.run();
        while (true) {
            try {
                receivePacket();
            } catch (SocketTimeoutException e) {
                continue;
            }
            deliverData(extractPayload());
            vmessage("got packet %d", received_packets);
            received_packets++;
            if (isEOF()) {
                break;
            }
        }
    }
}
