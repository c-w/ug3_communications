import java.net.SocketTimeoutException;
import java.util.TreeMap;

import udp.Server;


public class Receiver4 extends Server {

    /** currently expected but not yet received packet **/
    protected int rcv_base;
    /** local buffer (sorted in order of packet sequence number) **/
    protected TreeMap<Integer, byte[]> local_buffer;

    public static void main(String[] args) {
        Receiver4 r4 = new Receiver4(args);
        r4.run();
        r4.postRun();
    }

    public Receiver4(String[] args) {
        super(args);
        local_buffer = new TreeMap<Integer, byte[]>();
        rcv_base = 0;
    }

    /** receive packets according to the selective-repeat protocol **/
    @Override public void run() {
        super.run();
        int seq_num;
        while (true) {
            try {
                receivePacket();
            } catch (SocketTimeoutException e) {
                continue;
            }
            seq_num = extractSequenceNumber();
            if (seq_num >= rcv_base && seq_num <= rcv_base + window_size - 1) {
                // packet with number in [rcv_base, rcv_base+N-1]: ACK it
                vmessage("got new packet %d - ACK", seq_num);
                sendPacket(makeACK(seq_num));
                if (isEOF()) {
                    break;
                }
                if (!local_buffer.containsKey(seq_num)) {
                    // packet not previously received: need to buffer it
                    local_buffer.put(Integer.valueOf(seq_num), extractPayload());
                }
                if (seq_num == rcv_base) {
                    // received window base packet: deliver data
                    vmessage("deliver data");
                    for (byte[] bytes : local_buffer.values()) {
                        deliverData(bytes);
                        rcv_base++;
                    }
                    local_buffer.clear();
                }
            } else if (seq_num >= rcv_base - window_size && seq_num <= rcv_base - 1) {
                // packet with number in [rcv_base-N, rcv_base-1]: re-ACK it
                vmessage("re-ACK of %d", seq_num);
                sendPacket(makeACK(seq_num));
            } else {
                // otherwise ignore the packet
                vmessage("packet out of range, resending base %d", rcv_base);
                sendPacket(makeACK(rcv_base));
            }
        }
        // re-send the last acknowledgement a number of times and hope one
        // makes it through
        sendPacket(makeACK(rcv_base), NUM_EOF_ACKS);
    }
}
