package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public abstract class FileUtils {
    public static final int BUFFER_SIZE = 8192;

    public static byte[] file2bytes(File file) throws FileNotFoundException,
                                                      IOException {
        FileInputStream input_stream = null;
        ByteArrayOutputStream output_stream = null;
        byte[] bytes = null;
        try {
            input_stream = new FileInputStream(file);
            output_stream = new ByteArrayOutputStream();
            copyStream(input_stream, output_stream);
            bytes = output_stream.toByteArray();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } finally {
            if (input_stream != null) {
                input_stream.close();
                input_stream = null;
            }
            if (output_stream != null) {
                output_stream.close();
                output_stream = null;
            }
        }
        return bytes;
    }

    public static void bytes2file(byte[] bytes, File file) throws FileNotFoundException,
                                                                  IOException {
        ByteArrayInputStream input_stream = null;
        FileOutputStream output_stream = null;
        try {
            input_stream = new ByteArrayInputStream(bytes);
            output_stream = new FileOutputStream(file);
            copyStream(input_stream, output_stream);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } finally {
            if (input_stream != null) {
                input_stream.close();
                input_stream = null;
            }
            if (output_stream != null) {
                output_stream.close();
                output_stream = null;
            }
        }
    }

    public static void copyStream(InputStream is, OutputStream os) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int len;
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
    }
}
