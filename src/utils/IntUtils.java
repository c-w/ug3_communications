package utils;

public abstract class IntUtils {

    /**
     * Convert a byte-array to an integer
     */
    public static int bytes2int(byte[] bytes) {
        int rval = 0;
        for (int i = 0; i < bytes.length; i++) {
            rval = rval << 8;
            rval = rval | ((int) bytes[i] & 0xFF);
        }
        return rval;
    }

    /**
     * Convert an integer to a byte-array
     */
    public static byte[] int2bytes(int num) {
        return new byte[] {
            (byte) (num >>> 24),
            (byte) (num >>> 16),
            (byte) (num >>> 8),
            (byte) (num)
        };
    }
}
