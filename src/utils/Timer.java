package utils;

public class Timer {
    private long start_time;
    private long interval;
    private boolean is_running;

    public Timer(long delay) {
        interval = delay;
        is_running = false;
    }

    public Timer(int delay) {
        this((long) delay);
    }

    public boolean running() {
        return is_running;
    }

    public boolean timeout() {
        if (!is_running) {
            return false;
        }
        long now = System.currentTimeMillis();
        boolean timeout = now > start_time + interval;
        return timeout;
    }

    public void start() {
        if (!is_running) {
            start_time = System.currentTimeMillis();
            is_running = true;
        }
    }

    public void stop() {
        is_running = false;
    }
}
