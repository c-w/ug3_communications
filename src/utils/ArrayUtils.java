package utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public abstract class ArrayUtils {

    /**
     * Split an array into chunks
     * The last chunk may be smaller if the length of the array is not a
     * multiple of the chunk size
     */
    public static ArrayList<byte[]> array2chunks(byte[] input_array, int chunk_size) {
        ArrayList<byte[]> chunks = new ArrayList<byte[]>();
        int num_full_chunks = input_array.length / chunk_size;
        int input_ptr = 0;
        while (input_ptr < num_full_chunks * chunk_size) {
            byte[] chunk = new byte[chunk_size];
            for (int i = 0; i < chunk.length; i++) {
                chunk[i] = input_array[input_ptr];
                input_ptr++;
            }
            chunks.add(chunk);
        }
        int remaining_bytes = input_array.length - input_ptr;
        byte[] rest = new byte[remaining_bytes];
        for (int i = 0; i < rest.length; i++) {
            rest[i] = input_array[input_ptr];
            input_ptr++;
        }
        chunks.add(rest);
        return chunks;
    }

    /**
     * Combine parts into a single array
     */
    public static byte[] chunks2array(List<byte[]> arrays) {
        byte[] merged = new byte[numberOfElements(arrays)];
        int i = 0;
        for (byte[] array : arrays) {
            for (byte b : array) {
                merged[i] = b;
                i++;
            }
        }
        return merged;
    }

    /**
     * Compute the number of elements in the array
     */
    public static int numberOfElements(List<byte[]> list) {
        int len = 0;
        Iterator<byte[]> iterator = list.iterator();
        while (iterator.hasNext()) {
            len += iterator.next().length;
        }
        return len;
    }
}
