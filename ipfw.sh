#!/bin/bash

ipfw flush
ipfw add pipe 100 in
ipfw add pipe 200 out

case $1 in
    1)
        ipfw pipe 100 config delay 10ms plr 0.0 bw 10Mbits/s
        ipfw pipe 200 config delay 10ms plr 0.0 bw 10Mbits/s
        ;;
    2)
        ipfw pipe 100 config delay 10ms plr 0.05 bw 10Mbits/s
        ipfw pipe 200 config delay 10ms plr 0.05 bw 10Mbits/s
        ;;
    3-1)
        ipfw pipe 100 config delay 10ms plr 0.05 bw 10Mbits/s
        ipfw pipe 200 config delay 10ms plr 0.05 bw 10Mbits/s
        ;;
    3-2)
        ipfw pipe 100 config delay 100ms plr 0.05 bw 10Mbits/s
        ipfw pipe 200 config delay 100ms plr 0.05 bw 10Mbits/s
        ;;
    3-3)
        ipfw pipe 100 config delay 500ms plr 0.05 bw 10Mbits/s
        ipfw pipe 200 config delay 500ms plr 0.05 bw 10Mbits/s
        ;;
    4)
        ipfw pipe 100 config delay 100ms plr 0.05 bw 10Mbits/s
        ipfw pipe 200 config delay 100ms plr 0.05 bw 10Mbits/s
        ;;
    *)
        echo "missing argument - please specify task number"
        exit 1
        ;;
esac
